# Projet 3 - Openclassrooms #
-------------------------

CRÉEZ UN BLOG POUR UN ÉCRIVAIN

Un écrivain souhaite avoir son propre blog pour publier son prochain roman par étapes. Seul souci : il ne souhaite pas utiliser WordPress, il veut son propre blog maison !

## Installation ##


Cloner le repository

```
#!console

git clone git@bitbucket.org:jeranders/p3_blog.git

cd p3_blog

composer install

php bin/console doctrine:database:create

php bin/console d:s:u -f
```
  
ou

```
#!console

php bin/console doctrine:schema:update --force
```
  

Lancer le serveur: 

```
#!console

    php bin/console server:run

```



**Attention !**
Le premier utilisateur enregistré passe directement au rang "admin"