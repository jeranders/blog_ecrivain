<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Nom d\'utilisateur',
                'required' => true,
                'constraints' => array(
                    new Length(array(
                        'min' => 3,
                        'minMessage' => 'Le nom d\'utilisateur doit faire 3 caractères minimum',
                        'max' => 15,
                        'maxMessage' => 'Le nom d\'utilisateur doit faire 15 caractères maximum'
                    )),
                    new NotBlank(array(
                        'message' => 'Nom d\'utilisateur obligatoire'
                    )),
                    new NotNull(array(
                        'message' => 'Nom d\'utilisateur obligatoire'
                    ))
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Adresse email',
                'required' => true,
                'constraints' => array(
                    new Email(array(
                        'message' => 'L\'adresse email est invalide',
                        'checkMX' => true
                    )),
                    new NotBlank(array(
                        'message' => 'Adresse email obligatoire'
                    )),
                    new NotNull(array(
                        'message' => 'Adresse email obligatoire'
                    ))
                )
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Répéter le mot de passe'),
                'invalid_message' => 'Les mots de passe ne sont pas identique',
                'required' => true,
                'constraints' => array(
                    new Length(array(
                        'min' => 6,
                        'minMessage' => 'Le mot de passe doit faire 6 caractères minimum',
                        'max' => 255,
                        'maxMessage' => 'Le mot de passe doit faire 255 caractères maximum'
                    )),
                    new NotBlank(array(
                        'message' => 'Mot de passe obligatoire'
                    )),
                    new NotNull(array(
                        'message' => 'Mot de passe obligatoire'
                    ))
                )
            ))
            ->add('lastName', TextType::class, array(
                'label' => 'Nom',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Nom obligatoire'
                    )),
                    new NotNull(array(
                        'message' => 'Nom obligatoire'
                    ))
                )
            ))
            ->add('firstName', TextType::class, array(
                'label' => 'Prénomn',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Prénomn obligatoire'
                    )),
                    new NotNull(array(
                        'message' => 'Prénomn obligatoire'
                    ))
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user_bundle_register_type';
    }
}
