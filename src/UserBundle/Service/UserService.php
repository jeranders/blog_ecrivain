<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 15/04/2017
 * Heure: 12:24
 */

namespace UserBundle\Service;

use UserBundle\Entity\User;
use UserBundle\Form\RegisterType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class UserService
{
    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var UserPasswordEncoder
     */
    private $passEncoder;

    /**
     * @var AuthenticationUtils
     */
    private $loginUtils;

    /**
     * @var AuthorizationChecker
     */
    private $secuCheck;

    /**
     * @var TokenStorage
     */
    private $security;

    /**
     * UserService constructor.
     * @param EntityManager $doctrine
     * @param FormFactory $form
     * @param UserPasswordEncoder $passEncoder
     * @param AuthenticationUtils $loginUtils
     * @param AuthorizationChecker $secuCheck
     * @param TokenStorage $security
     */
    public function __construct(EntityManager $doctrine, FormFactory $form, UserPasswordEncoder $passEncoder, AuthenticationUtils $loginUtils, AuthorizationChecker $secuCheck, TokenStorage $security)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->passEncoder = $passEncoder;
        $this->loginUtils = $loginUtils;
        $this->secuCheck = $secuCheck;
        $this->security = $security;
    }

    /**
     * Système d'inscription du blog
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function registerUser(Request $request)
    {
        $checkUser = $this->doctrine->getRepository('UserBundle:User')->findAll();

        $user = new User();

        $form = $this->form->create(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $password = $this->passEncoder->encodePassword(
                $user,
                $user->getPassword()
            );

            $user->setPassword($password);

            if (count($checkUser) == '')
            {
                $user->setRoles(['ROLE_ADMIN']);
            }else{
                $user->setRoles(['ROLE_USER']);
            }

            $this->doctrine->persist($user);
            $this->doctrine->flush();
        }

        return $form;

    }

    /**
     * @return array
     */
    public function loginUser()
    {
        return array(
            '_username' => $this->loginUtils->getLastUsername(),
            'error' => $this->loginUtils->getLastAuthenticationError(),
        );
    }

    /**
     * @return array
     */
    public function threeLastUsersService()
    {
        $users = $this->doctrine->getRepository('UserBundle:User')->threeLastUsers();

        return $users;
    }

}