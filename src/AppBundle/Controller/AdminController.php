<?php
/**
 * User: J�r�me Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 17/04/2017
 * Heure: 09:28
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class AdminController extends Controller
{

    /**
     * Accueil page d'aministration
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("admin/", name="admin")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        $countUser      = $this->get('app.admin')->countUser();
        $countWarn      = $this->get('app.comment')->countWarn();
        $countBillet    = $this->get('app.billet')->countBillet();
        $countComments  = $this->get('app.comment')->countComments();

        $comments       = $this->get('app.comment')->threeLastCommentsService();
        $billets        = $this->get('app.billet')->threeLastBilletsService();
        $users          = $this->get('app.user')->threeLastUsersService();
        $warns          = $this->get('app.comment')->threeLastWarnService();

        return $this->render('admin/index.html.twig',
            array(
                'countUser' => $countUser,
                'countWarn' => $countWarn,
                'countBillet' => $countBillet,
                'countComments' => $countComments,
                'comments' => $comments,
                'billets' => $billets,
                'users' => $users,
                'warns' => $warns
            ));
    }

    /**
     * Liste des membres
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("admin/liste-membres", name="listUsers")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listUsersAction()
    {
        $listInfoUsers = $this->get('app.admin')->listInfoUsers();

        return $this->render('admin/listUsers.html.twig',
            array(
                'listInfoUsers' => $listInfoUsers
            ));
    }

    /**
     * Supprimer un membre
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("admin/liste-membres/{id}/delete_user", requirements={"id": "\d+"}, name="delete_user")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteUserAction($id)
    {
        $deleteService = $this->get('app.admin');
        $user          = $deleteService->findOneUser(['id' => $id]);
        $deleteService->deleteUser($user);

        $this->addFlash('success', 'Membre bien supprim�');

        return $this->redirectToRoute('listUsers');
    }

    /**
     * Voir le profil d'un membre
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("admin/liste-membres/{id}/show_user", requirements={"id": "\d+"}, name="show_user")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showUserAction($id)
    {
        $showUser = $this->get('app.admin')->findOneUser(['id' => $id]);

        return $this->render('admin/showUser.html.twig',
            array(
                'showUser' => $showUser
            ));
    }

    /*****************************************************
     * Gestion des billets
     *****************************************************/




}