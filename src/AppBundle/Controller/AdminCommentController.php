<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 18/05/2017
 * Heure: 12:16
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminCommentController extends Controller
{
    /**
     * Liste des signalements
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("admin/warnList", name="warnList")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function warnListAction()
    {
        $list = $this->get('app.comment')->listWarnComment();

        return $this->render('admin/warnlist.html.twig',
            array(
                'listWarn' => $list
            ));
    }

    /**
     * Liste les commentaires
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/listComments", name="listComments")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listCommentsAction()
    {
        $list = $this->get('app.comment')->listComments();

        return $this->render('admin/listComments.html.twig',
            array(
                'listComments' => $list
            ));
    }

    /**
     * Supprime un commentaire
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("admin/warnList/{id}/delete_comment", requirements={"id": "\d+"}, name="warnList_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteWarnAction($id)
    {
        $deleteServiceWarn = $this->get('app.comment');
        $warn = $deleteServiceWarn->findOneWarn($id);
        $deleteServiceWarn->deleteWarn($warn);

        $this->addFlash('success', 'Commentaire bien supprimé');

        return $this->redirectToRoute('warnList');
    }

    /**
     * Annule un signalement
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("admin/warnList/{id}/warn_cancel", requirements={"id": "\d+"}, name="warn_cancel")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function cancelWarnCommentAction($id)
    {
        $warn = $this->get('app.comment');
        $warn->cancelWarnComment($id);

        $this->addFlash('success', 'Le commentaire n\'est plus considéré comme signalé');

        return $this->redirectToRoute('warnList');
    }
}