<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Billet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Accueil du site
     *
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("{page}", name="homepage", defaults={"page": 1})
     */
    public function indexAction($page)
    {
        if ($page < 1) {
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }

        $nbPerPage = 2;

        $listBillets = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Billet')
            ->getBillets($page, $nbPerPage)
        ;

        $nbPages = ceil(count($listBillets) / $nbPerPage);

        $countBillet = $this->get('app.billet')->countBilletPublish();

        if ($countBillet != 0)
        {
            if ($page > $nbPages) {
                throw $this->createNotFoundException("La page ".$page." n'existe pas. :(");
            }
        }
        return $this->render('default/index.html.twig',
            array(
                'countBillet' => $countBillet,
                'listBillets' => $listBillets,
                'nbPages'     => $nbPages,
                'page'        => $page,
            ));
    }

    /**
     * Affichage d'un billet avec les commentaires
     *
     * @param Request $request
     * @param $id
     * @param $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route("show-billet/{id}/{page}", requirements={"id": "\d+"}, name="show_billet_front", defaults={"page": 1})
     */
    public function showBilletAction(Request $request, $id, $page)
    {
        $showBillet = $this->get('app.billet')->findOneBillet(['id' => $id]);

        $valide = $this->get('app.billet')->billetPublishValide($id);
        $comment = $this->get('app.comment')->addComment($request, $showBillet);
        $countComments = $this->get('app.comment')->countCommentBillet($showBillet);

        if ($comment->isSubmitted() && $comment->isValid()) {
            $this->addFlash('success', 'Merci pour votre commentaire ');
            return $this->redirectToRoute(
                'show_billet_front',
                array('id' => $showBillet->getId())
            );
        }

        if ($valide == 0)
        {
            return $this->redirectToRoute('homepage');
        }

        $nbPerPage = 2;

        $listComments = $this->get('app.comment')->getComments($showBillet, $page, $nbPerPage);

        $nbPages = ceil(count($listComments) / $nbPerPage);

        if ($countComments != 0)
        {
            if ($page < 1) {
                throw $this->createNotFoundException("La page ".$page." n'existe pas.");
            }
            if ($page > $nbPages) {
                throw $this->createNotFoundException("La page ".$page." n'existe pas.");
            }
        }

        $replyComment = $this->get('app.comment')->replyCommentService($request, $showBillet);

        if ($replyComment->isSubmitted() && $replyComment->isValid())
        {
            $this->addFlash('success', 'Merci pour votre réponse au commentaire ');
            return $this->redirectToRoute('show_billet_front', array('id' => $showBillet->getId()));

        }

        $showReplyComment = $this->get('app.comment')->getReplyComment();

        return $this->render('default/showBillet.html.twig',
            array(
                'showBillet'       => $showBillet,
                'form'             => $comment->createView(),
                'countComments'    => $countComments,
                'listComments'     => $listComments,
                'nbPages'          => $nbPages,
                'page'             => $page,
                'replyComment'     => $replyComment->createView(),
                'showReplyComment' => $showReplyComment
            ));
    }

    /**
     * Suppression d'un billet
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("show-billet/{id}/delete_comment_user/", requirements={"id": "\d+"}, name="delete_comment_user")
     */
    public function deleteCommentUserAction($id)
    {
        $deleteService = $this->get('app.comment');
        $comment = $deleteService->findOneComment($id);
        $billet = $comment->getBillet()->getId();
        $deleteService->deleteCommentBilletUser($comment);

        $this->addFlash('success', 'Votre commentaire est bien supprimé');

        return $this->redirectToRoute('show_billet_front', array('id' => $billet));
    }

    /**
     * Suppression d'un billet
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("show-billet/{id}/delete_comment_user_all/", requirements={"id": "\d+"}, name="delete_comment_user_all")
     */
    public function deleteCommentUserAllAction($id)
    {
        $deleteService = $this->get('app.comment');
        $replyComment = $deleteService->findOneComment($id);
        $deleteService->deleteCommentBilletUserAll($replyComment);

        $comment = $deleteService->findOneComment($id);
        $billet = $comment->getBillet()->getId();

        $deleteService->deleteCommentBilletUser($comment);

        $this->addFlash('success', 'Votre commentaire est bien supprimé');

        return $this->redirectToRoute('show_billet_front', array('id' => $billet));
//        return $this->redirectToRoute('homepage');
    }

    /**
     * Signaler un commentaire
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("comment/{id}/warn", requirements={"id": "\d+"}, name="warn")
     */
    public function warnCommentAction($id)
    {
        $billet = $this->get('app.comment')->findOneComment($id)->getBillet()->getId();
        $warn = $this->get('app.comment');
        $warn->warnComment($id);


        $this->addFlash('success', 'Vous avez signalez le commentaire');

        return $this->redirectToRoute('show_billet_front', array('id' => $billet));
    }

    /**
     * Formulaire de contact
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("contact/", name="contact_form")
     */
    public function contactAction(Request $request)
    {
        $contact = $this->get('app.contact')->contact($request);

        if ($contact->isSubmitted() && $contact->isValid())
        {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/contact.html.twig',
            array(
                'contact' => $contact->createView()
            ));
    }

    /**
     * Table des matières
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("table/", name="table_contents")
     */
    public function tableContentsAction()
    {
        $billets = $this->get('app.billet')->listBilletASC();
        $countBillet = $this->get('app.billet')->countBilletPublish();

        return $this->render('default/table_des_matieres.html.twig',
            array(
                'billets' => $billets,
                'countBillet' => $countBillet
            ));
    }
}
