<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 18/05/2017
 * Heure: 12:19
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class AdminBilletController extends Controller
{
    /**
     * Ajouter un billet
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("admin/ajout_billet", name="addBillet")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addBilletAction(Request $request)
    {

        $form = $this->get('app.billet')->addBillet($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->addFlash('success', 'Billet bien ajouté ');
            return $this->redirectToRoute('addBillet');
        }

        return $this->render('admin/ajout_billet.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Liste des billets
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("admin/list-Billet", name="listBillet")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listBilletAction()
    {
        $listBillet = $this->get('app.billet')->listBillet();

        return $this->render('admin/listBillet.html.twig',
            array(
                'listBillet' => $listBillet
            ));
    }

    /**
     * Voir le billet
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("admin/list-Billet/{id}/show_billet", requirements={"id": "\d+"}, name="show_billet")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showBilletAction($id)
    {
        $showBillet = $this->get('app.billet')->findOneBillet(['id' => $id]);

        return $this->render('admin/showBillet.html.twig',
            array(
                'showBillet' => $showBillet
            ));
    }

    /**
     * Supprimer un billet
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("admin/list-Billet/{id}/delete_billet", requirements={"id": "\d+"}, name="delete_billet")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteBilletAction($id)
    {
        $deleteService = $this->get('app.billet');
        $billet = $deleteService->findOneBillet($id);
        $deleteService->deleteBillet($billet);

        $this->addFlash('success', 'Billet bien supprimé');

        return $this->redirectToRoute('listBillet');
    }

    /**
     * Mise a jour d'un billet
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("admin/list-Billet/{id}/edit_billet", requirements={"id": "\d+"}, name="edit_billet")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateBilletAction(Request $request, $id)
    {
        $billet = $this->get('app.billet')->findOneBillet($id);

        if (!$billet) {
            $this->addFlash('danger', 'Le billet demandé est inconnu');
            return $this->redirectToRoute('homepage');
        }
        $form = $this->get('app.billet')->updateBillet($request, $id);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('listBillet');
        }

        return $this->render('admin/editBillet.html.twig',
            array(
                'billet' => $billet,
                'form' => $form->createView()
            ));
    }
}