<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', TextareaType::class, array(
                'label' => 'Votre commentaire',
                'constraints' => array(
                    new Length(array(
                        'min' => 20,
                        'minMessage' => 'Votre commentaire doit faire 20 caractères minimum',
                        'max' => 255,
                        'maxMessage' => 'Votre commentaire doit faire 255 caractères maximum',
                    ))
                )
            ))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Comment'
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_comment_type';
    }
}
