<?php
/**
 * Created by PhpStorm.
 * User: Jeranders
 * Date: 10/05/2017
 * Time: 14:19
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                'required' => true
            ))
            ->add('name', TextType::class, array(
                'required' => true
            ))
            ->add('phone', TextType::class, array(
                'required' => false
            ))
            ->add('subject', TextType::class, array(
                'required' => true
            ))
            ->add('content', TextareaType::class, array(
                'required' => true
            ))
        ;
    }
}