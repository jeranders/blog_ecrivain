<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 12/05/2017
 * Heure: 18:28
 */

namespace AppBundle\Form;


use AppBundle\Repository\BilletRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class ReplyCommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', TextareaType::class, array(
                'label' => 'Votre commentaire',
                'constraints' => array(
                    new Length(array(
                        'min' => 20,
                        'minMessage' => 'Votre commentaire doit faire 20 caractères minimum',
                        'max' => 255,
                        'maxMessage' => 'Votre commentaire doit faire 255 caractères maximum',
                    ))
                )
            ))
            ->add('parent', HiddenType::class)
//            ->add('billet', HiddenType::class, array(
//                    'query_builder' => function(BilletRepository $cr) {
//                        return $cr->find();
//                    }
//            ))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Comment'
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_comment_type_reply';
    }
}