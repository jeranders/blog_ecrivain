<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 23/04/2017
 * Heure: 10:05
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class BilletFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,array(
                'label' => 'Titre',
                'constraints' => array(
                    new Length(array(
                        'min' => 5,
                        'minMessage' => 'Le titre doit contenir 5 caractères minimum',
                        'max' => 255,
                        'maxMessage' => 'Le titre doit contenir 255 caractères maximum'
                    ))
                )
            ))
            ->add('content', TextareaType::class,array(
                'label' => 'Contenu',
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                    new NotNull(),
                    new Length(array(
                        'min' => 1000,
                        'minMessage' => 'Votre texte doit avoir minimum 1000 caractères'
                    ))
                )
            ))
            ->add('publish', CheckboxType::class,array(
                'label' => 'Publication',
                'required' => false,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Billet'
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_billet_form_type';
    }
}
