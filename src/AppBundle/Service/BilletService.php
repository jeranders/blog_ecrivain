<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 23/04/2017
 * Heure: 10:02
 */

namespace AppBundle\Service;


use AppBundle\Entity\Billet;
use AppBundle\Form\BilletFormType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class BilletService
{
    /**
     * @var EntityManager
     */
    private $doctrine;
    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var TokenStorage
     */
    private $security;

    /**
     * BilletService constructor.
     * @param EntityManager $doctrine
     * @param FormFactory $form
     * @param TokenStorage $security
     */
    public function __construct(EntityManager $doctrine, FormFactory $form, TokenStorage $security)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->security = $security;
    }

    /**
     * Ajout d'un billet
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addBillet(Request $request)
    {
        $billet = new Billet();

        $form = $this->form->create(BilletFormType::class, $billet);

        $user = $this->security->getToken()->getUser();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $billet->setUser($user);

            $this->doctrine->persist($billet);
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Dernier billet enregistré
     *
     * @return array
     */
    public function lastBillet()
    {
        $lastBillet = $this->doctrine->getRepository('AppBundle:Billet')->lastBillet();
        return $lastBillet;
    }

    /**
     * Recherche d'un billet par ID
     *
     * @param $id
     * @return Billet|null|object
     */
    public function findOneBillet($id)
    {
        $billet = $this->doctrine->getRepository('AppBundle:Billet')->find($id);
        return $billet;
    }

    /**
     * Liste des billets
     *
     * @return Billet[]|array
     */
    public function listBillet()
    {
        $listInfoUsers = $this->doctrine->getRepository('AppBundle:Billet')->listBilletByDate();

        return $listInfoUsers;
    }

    public function listBilletASC()
    {
        $listInfoUsers = $this->doctrine->getRepository('AppBundle:Billet')->listBilletByDateASC();

        return $listInfoUsers;
    }

    /**
     * Suppresion d'un billet
     *
     * @param $id
     */
    public function deleteBillet($id)
    {
        $this->doctrine->remove($id);
        $this->doctrine->flush();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\Form\FormInterface
     */
    public function updateBillet(Request $request, $id)
    {
        $billet = $this->doctrine
            ->getRepository('AppBundle:Billet')
            ->findOneBy(array('id' => $id));
        $form = $this->form->create(BilletFormType::class, $billet);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Compte le nombre de billet avec publication à 1
     * @return mixed
     */
    public function countBilletPublish()
    {
        $countBillet = $this->doctrine->getRepository('AppBundle:Billet')->countBilletPublish();
        return $countBillet;
    }

    /**
     * Compte le nombre de billet
     * @return mixed
     */
    public function countBillet()
    {
        $countBillet = $this->doctrine->getRepository('AppBundle:Billet')->countBillet();
        return $countBillet;
    }

    /**
     * Billet avec publication à 1 par rapport à l'id
     * @param $id
     * @return mixed
     */
    public function billetPublishValide($id)
    {
        $valide = $this->doctrine->getRepository('AppBundle:Billet')->billetPublishValide($id);
        return $valide;
    }

    public function threeLastBilletsService()
    {
        $comments = $this->doctrine->getRepository('AppBundle:Billet')->threeLastBillets();

        return $comments;
    }
}