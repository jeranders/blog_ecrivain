<?php
/**
 * Created by PhpStorm.
 * User: Jeranders
 * Date: 10/05/2017
 * Time: 14:16
 */

namespace AppBundle\Service;

use AppBundle\Form\ContactType;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ContactService
{
    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var \Swift_Mailer
     */
    private $mail;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @var Session
     */
    private $session;

    /**
     * ContactService constructor.
     * @param FormFactory $form
     * @param \Swift_Mailer $mail
     * @param TwigEngine $templating
     * @param Session $session
     */
    public function __construct(FormFactory $form, \Swift_Mailer $mail, TwigEngine $templating, Session $session)
    {
        $this->form = $form;
        $this->mail = $mail;
        $this->templating = $templating;
        $this->session = $session;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function contact(Request $request)
    {
        $form = $this->form->create(ContactType::class);

        $form->handleRequest($request);

        if ($request->getMethod() == 'POST')
        {
            if ($form->isValid() && $form->isSubmitted())
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject($form['subject']->getData())
                    ->setFrom($form['email']->getData())
                    ->setTo('brechoire.j@gmail.com')
                    ->setBody(
                        $this->templating->render('Email/contact.html.twig', array(
                            'content' => $form['content']->getData(),
                            'name' => $form['name']->getData(),
                            'email' => $form['email']->getData(),
                            'phone' => $form['phone']->getData(),
                            'subject' => $form['subject']->getData()
                        )),
                        'text/html'
                    );
                $this->mail->send($message);
                $this->session->getFlashBag()->add('success', 'Votre message a bien été envoyé');
            }
        }

        return $form;

    }
}