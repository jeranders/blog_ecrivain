<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 17/04/2017
 * Heure: 10:45
 */

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;

/**
 * Class AdminService
 * @package AppBundle\Service
 *
 * Gestion de l'administration
 */
class AdminService
{
    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * @var FormFactory
     */
    private $form;

    /**
     * AdminService constructor.
     * @param EntityManager $doctrine
     * @param FormFactory $form
     */
    public function __construct(EntityManager $doctrine, FormFactory $form)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
    }

    /**
     * Compte le nombre d'utilisateur
     *
     * @return mixed
     */
    public function countUser()
    {
        $countUser = $this->doctrine->getRepository('UserBundle:User')->countUser();

        return $countUser;
    }

    /**
     * Récupère la liste des membres
     *
     * @return array
     */
    public function listInfoUsers()
    {
        $listInfoUsers = $this->doctrine->getRepository('UserBundle:User')->listInfoUsers();

        return $listInfoUsers;
    }

    /**
     * Recherche utilisateur via ID
     * @param $id
     * @return null|object
     */
    public function findOneUser($id)
    {
        $user = $this->doctrine->getRepository('UserBundle:User')->findOneBy($id);
        return $user;
    }

    /**
     * Suppresion d'un utilisateur
     * @param $id
     */
    public function deleteUser($id)
    {
        $this->doctrine->remove($id);
        $this->doctrine->flush();
    }


}