<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 26/04/2017
 * Heure: 22:00
 */

namespace AppBundle\Service;

use AppBundle\Entity\Billet;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentReplyType;
use AppBundle\Form\CommentType;
use AppBundle\Form\ReplyCommentType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CommentService
{
    /**
     * @var EntityManager
     */
    private $doctrine;
    /**
     * @var FormFactory
     */
    private $form;

    /**
     * @var TokenStorage
     */
    private $security;

    /**
     * CommentService constructor.
     * @param EntityManager $doctrine
     * @param FormFactory $form
     * @param TokenStorage $security
     */
    public function __construct(EntityManager $doctrine, FormFactory $form, TokenStorage $security)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->security = $security;
    }

    /**
     * Ajout d'un commentaire
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\Form\FormInterface
     */
    public function addComment(Request $request, $id)
    {
        $comment = new Comment();

        $form = $this->form->create(CommentType::class, $comment);

        $form->handleRequest($request);

        $user = $this->security->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setBillet($id);
            $comment->setUser($user);

            $this->doctrine->persist($comment);
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Répondre à un commentaire
     *
     * @param Request $request
     * @param $idBillet
     * @return \Symfony\Component\Form\FormInterface
     */
    public function replyCommentService(Request $request, $idBillet)
    {
        $reply = new Comment();
        $form = $this->form->create(ReplyCommentType::class, $reply);
        $user = $this->security->getToken()->getUser();
        $form->handleRequest($request);
        $idComment = intval($reply->getParent());

        if ($form->isSubmitted() && $form->isValid()) {

            $reply->setBillet($idBillet);
            $reply->setUser($user);

            $reply->setParent($idComment);
            $this->doctrine->persist($reply);
            $this->doctrine->flush();
        }

        return $form;
    }

    /**
     * Compte le nombre de commentaire d'un billet
     *
     * @param $id
     * @return mixed
     */
    public function countCommentBillet($id)
    {
        $comments = $this->doctrine->getRepository('AppBundle:Comment')->countCommentsBillet($id);
        return $comments;
    }

    /**
     * Supprime le commentaire d'un utilisateur
     *
     * @param $id
     */
    public function deleteCommentBilletUser($id)
    {
        $this->doctrine->remove($id);
        $this->doctrine->flush();
    }

    /**
     * Recherche un commentaire via l'id
     *
     * @param $id
     * @return Comment|null|object
     */
    public function findOneComment($id)
    {
        $billet = $this->doctrine->getRepository('AppBundle:Comment')->find($id);
        return $billet;
    }

    /**
     * Recherche un commentaire par son id sur la colonne parent_id
     * Permet de supprimer les enfants d'un commentaire
     *
     * @param $id
     * @return Comment[]|array
     */
    public function findOneCommentBy($id)
    {
        $billet = $this->doctrine->getRepository('AppBundle:Comment')->findBy(array('parent' => $id));
        return $billet;
    }

    /**
     * Supprime un commentaire avec les enfants de celui-ci
     *
     * @param $id
     */
    public function deleteCommentBilletUserAll($id)
    {
        $replyComment = $this->findOneCommentBy($id);

        foreach ($replyComment as $reply) {
            $this->doctrine->remove($reply);
        }

        $this->doctrine->flush();
    }

    /**
     * Signal un commentaire
     *
     * @param $id
     * @return Comment|null|object
     */
    public function warnComment($id)
    {
        $comment = $this->doctrine
            ->getRepository('AppBundle:Comment')
            ->findOneBy(array('id' => $id));

        $comment->setWarn(1);
        $this->doctrine->flush();

        return $comment;
    }

    /**
     * @param $id
     * @return Comment|null|object
     */
    public function cancelWarnComment($id)
    {
        $comment = $this->doctrine
            ->getRepository('AppBundle:Comment')
            ->findOneBy(array('id' => $id));

        $comment->setWarn(0);
        $this->doctrine->flush();

        return $comment;
    }

    /**
     * Compte le nombre de signalement
     *
     * @return mixed
     */
    public function countWarn()
    {
        $count = $this->doctrine->getRepository('AppBundle:Comment')->countWarn();
        return $count;
    }

    /**
     * Compte le nombre de commentaires
     *
     * @return mixed
     */
    public function countComments()
    {
        $count = $this->doctrine->getRepository('AppBundle:Comment')->countComments();
        return $count;
    }

    /**
     * Liste des signalements
     *
     * @return array
     */
    public function listWarnComment()
    {
        $listInfoUsers = $this->doctrine->getRepository('AppBundle:Comment')->warmList();

        return $listInfoUsers;
    }

    /**
     * Liste les commentaires
     *
     * @return Comment[]|array
     */
    public function listComments()
    {
        $list = $this->doctrine->getRepository('AppBundle:Comment')->findAll();
        return $list;
    }


    /**
     * Affiche les 3 derniers commentaires
     *
     * @return array
     */
    public function threeLastCommentsService()
    {
        $comments = $this->doctrine->getRepository('AppBundle:Comment')->threeLastComments();

        return $comments;
    }

    /**
     * Affiche les 3 derniers signalements
     *
     * @return array
     */
    public function threeLastWarnService()
    {
        $comments = $this->doctrine->getRepository('AppBundle:Comment')->threeLastWarn();

        return $comments;
    }

    /**
     * Recherche un signalement par son id
     *
     * @param $id
     * @return mixed
     */
    public function findOneWarn($id)
    {
        $warn = $this->doctrine->getRepository('AppBundle:Comment')->find($id);
        return $warn;
    }

    /**
     * Supprime un signalement
     *
     * @param $id
     */
    public function deleteWarn($id)
    {
        $this->doctrine->remove($id);
        $this->doctrine->flush();
    }

    /**
     * Affiche les réponses à un commentaire
     *
     * @return array
     */
    public function getReplyComment()
    {
        $replyComment = $this->doctrine->getRepository('AppBundle:Comment')->getReplyComments();
        return $replyComment;
    }

    /**
     * @param $showBillet
     * @param $page
     * @param $nbPerPage
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getComments($showBillet, $page, $nbPerPage)
    {
        $getComments = $this->doctrine->getRepository('AppBundle:Comment')->getComments($showBillet, $page, $nbPerPage);
        return $getComments;
    }
}