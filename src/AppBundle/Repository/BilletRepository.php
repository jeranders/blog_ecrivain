<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 23/04/2017
 * Heure: 10:03
 */

namespace AppBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

class BilletRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Dernier billet enregistré
     * @return mixed
     */
    public function lastBillet()
    {
        return $this->createQueryBuilder('b')
            ->select('b.title')
            ->orderBy('b.dateCreated', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Compte le nombre de billet avec une publication à 1
     * @return mixed
     */
    public function countBilletPublish()
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b.id)')
            ->where('b.publish = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Compte le nombre de billet
     * @return mixed
     */
    public function countBillet()
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Compte le nombre de billet avec publication à 1 par rapport à l'id
     * @param $id
     * @return mixed
     */
    public function billetPublishValide($id)
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b.id)')
            ->where('b.publish = 1')
            ->andWhere('b.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Liste des billets classé par date
     *
     * @return array
     */
    public function listBilletByDate()
    {
        return $this->createQueryBuilder('b')
            ->orderBy('b.dateCreated', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Liste des billets classé par date ASC
     * @return array
     */
    public function listBilletByDateASC()
    {
        return $this->createQueryBuilder('b')
            ->orderBy('b.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Liste des billets classé par date et avec une pagination
     *
     * @param $page
     * @param $nbPerPage
     * @return Paginator
     */
    public function getBillets($page, $nbPerPage)
    {
        $query = $this->createQueryBuilder('b')
            ->orderBy('b.dateCreated', 'DESC')
            ->getQuery()
        ;
        $query
            ->setFirstResult(($page-1) * $nbPerPage)
            ->setMaxResults($nbPerPage)
        ;
        return new Paginator($query, true);
    }

    /**
     * @return array
     */
    public function threeLastBillets()
    {
        return $this->createQueryBuilder('b')
            ->select('b.title')
            ->addSelect('b.id')
            ->addSelect('b.dateCreated')
            ->orderBy('b.dateCreated', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    public function findBillet($id)
    {
        return $this->createQueryBuilder('b')
            ->select('b.id')
            ->where('b.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}
