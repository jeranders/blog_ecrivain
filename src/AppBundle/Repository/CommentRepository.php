<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 26/04/2017
 * Heure: 21:08
 */

namespace AppBundle\Repository;


use Doctrine\ORM\Tools\Pagination\Paginator;

class CommentRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Compte le nombre de commentaire
     *
     * @return mixed
     */
    public function countComments()
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Liste des commentaires d'un billet
     *
     * @param $id
     * @return array
     */
    public function listCommentBillet($id)
    {
        return $this->createQueryBuilder('c')
            ->where('c.billet = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * Compte le nombre de commentaires sur un billet
     *
     * @param $id
     * @return mixed
     */
    public function countCommentsBillet($id)
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('c.billet = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Compte le nombre de signalement
     *
     * @return mixed
     */
    public function countWarn()
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('c.warn = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Liste des commentaires signalés
     *
     * @return array
     */
    public function warmList()
    {
        return $this->createQueryBuilder('c')
            ->where('c.warn = 1')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @param $page
     * @param $nbPerPage
     * @return Paginator
     */
    public function getComments($id, $page, $nbPerPage)
    {
        $query = $this->createQueryBuilder('c')
            ->where('c.billet = :id')
            ->andWhere('c.parent is NULL')
            ->setParameter('id', $id)
            ->getQuery()
        ;
        $query
            ->setFirstResult(($page-1) * $nbPerPage)
            ->setMaxResults($nbPerPage)
        ;
        return new Paginator($query, true);
    }

    /**
     * @return array
     */
    public function getReplyComments()
    {
        return $this->createQueryBuilder('c')
            ->where('c.parent IS NOT NULL')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return array
     */
    public function threeLastComments()
    {
        return $this->createQueryBuilder('c')
            ->select('c.comment')
            ->addSelect('c.id')
            ->addSelect('c.date')
            ->orderBy('c.date', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    public function threeLastWarn()
    {
        return $this->createQueryBuilder('c')
            ->select('c.id')
            ->addSelect('c.comment')
            ->addSelect('c.date')
            ->where('c.warn = 1')
            ->orderBy('c.date', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }
}